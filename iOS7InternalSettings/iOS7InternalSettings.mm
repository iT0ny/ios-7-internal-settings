//
//  iOS7InternalSettings.mm
//  iOS7InternalSettings
//
//  Created by Anton Titkov on 03/07/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <objc/runtime.h>

__attribute__((constructor)) void myInit();
void __setOrientationLocked (id self, SEL _cmd,BOOL tapped);
BOOL _isInternalInstall (id self, SEL _cmd);


BOOL _isInternalInstall (id self, SEL _cmd)
{
    return YES;
}

void __setOrientationLocked (id self, SEL _cmd,BOOL tapped)
{
    id prototype=[objc_getClass("SBPrototypeController") performSelector:@selector(sharedInstance)];
    [prototype performSelector:@selector(_showSettings)];
}

void myInit()
{
    
    class_addMethod(objc_getClass("SBPlatformController"), @selector(_isInternalInstall), (IMP)_isInternalInstall, "B@:");
    class_addMethod(objc_getClass("SBCCSettingsSectionController"), @selector(__setOrientationLocked:), (IMP)__setOrientationLocked, "v@:B");

    method_exchangeImplementations(class_getInstanceMethod(objc_getClass("SBPlatformController"), @selector(isInternalInstall)), class_getInstanceMethod(objc_getClass("SBPlatformController"), @selector(_isInternalInstall)));
    method_exchangeImplementations(class_getInstanceMethod(objc_getClass("SBCCSettingsSectionController"), @selector(_setOrientationLocked:)), class_getInstanceMethod(objc_getClass("SBCCSettingsSectionController"), @selector(__setOrientationLocked:)));

}
